import { useCallback, useEffect, useState } from 'react';
import './App.css';
import SingleCard from './components/SingleCard';

// array of card images
const cardImagesCompetences = [
  { "src": "/img/languages/css.png", matched: false, explanation: "CSS", description: "Il s'agit d'un langage utilisé dans le développement web pour la présentation des documents HTML. C'est un langage que je maitrise particulièrement au vu du nombre de projet que j'ai pu réaliser avec. Il est souvent associé au langage HTML." },
  { "src": "/img/languages/html.png", matched: false, explanation: "HTML", description: "Il s'agit d'un langage utilisé dans le développement web permettant la structuration des pages web. C'est un langage que je maitrise particulièrement au vu du nombre de projet que j'ai pu réaliser avec. Il est souvent associé au langage CSS." },
  { "src": "/img/languages/php.png", matched: false, explanation: "PHP", description: "Il s'agit d'un langage de programmation orienté objet utilisé essentiellement dans le développement web. J'ai utilisé ce langage pour réaliser plusieurs sites web comme celui d'une agence immobilière ou encore plus récemment, dans le cadre de mon alternance, une application permettant de saisir des données dans une base de données sur les abeilles." },
  { "src": "/img/languages/java.png", matched: false, explanation: "JAVA", description: "Il s'agit d'un langage de programmation orienté objet. Ce langage m'a permis de réaliser des applications de gestion bancaire ou encore de gestion de consomation électrique." },
  { "src": "/img/languages/python.png", matched: false, explanation: "PYTHON", description: "Il s'agit d'un langage de programmation interprété. Avec ce langage, j'ai pu développer une application pour une agence immobilière ou encore une application de chat en réseau." },
  { "src": "/img/languages/javascript.png", matched: false, explanation: "JAVASCRIPT", description: "Il s'agit d'un langage de programmation utilisé dans le développement web permettant la dynamisation et l'intéractivité des pages web. Grace à ce langage, j'ai pu développer des applications web comme un jeu de carte en ligne, une application web permettant de donner de la visibilité aux associations universitaires ou encore cette application même."},
  { "src": "/img/languages/bootstrap.png", matched: false, explanation: "Bootstrap", description: "Il s'agit d'une collection d'outils utiles à la création du design de sites et d'applications web."},
  { "src": "/img/languages/symfony.png", matched: false, explanation: "Symfony", description: "Il s'agit d'un framework PHP permettant de faciliter et d’accélérer le développement d'un site web. J'ai pu utiliser ce framework au cours de scolaire et professionnel, notemment pour créer le site web d'une agence immobilère, mais aussi afin de gérer une application permettant de saisir des données dans une base de données sur les abeilles." },
  { "src": "/img/languages/reactjs.png", matched: false, explanation: "ReactJS", description: "Il s'agit d'une bibliothèque JavaScript qui a pour but principal de faciliter la création d'application web monopage. Avec cette bibliotèque, j'ai pu développer des applications en Javascript comme cette application même." },
  { "src": "/img/languages/sql.png", matched: false, explanation: "SQL", description: "Il s'agit d'un langage informatique normalisé servant à exploiter des bases de données relationnelles. J'ai pu utiliser ce langage dans de nombreux projets comme tous ceux qui nécessitaient une base de données SQL."},
]

const cardImagesParcours = [
  { "src": "/img/parcours/lycee.png", matched: false, explanation: "Lycée Henri Matisse", description: "Durant ma scolarité au lycée, j'ai pu accroître mon goût pour les matières scientifiques notemment l'informatique et les mathématiques. En 2018, j'ai obtenu mon Baccalauréat Scientifique spécialité mathématiques avec mention Assez-Bien." },
  { "src": "/img/parcours/iut.jpg", matched: false, explanation: "IUT de Blagnac", description: "Je me suis spécialisé dans l'informatique à partir de l'enseignement supérieur. En 2020, j'ai obtenu mon DUT Informatique que j'ai réalisé à l'IUT de Blagnac." },
  { "src": "/img/parcours/UT2J_licence.png", matched: false, explanation: "Licence MIASHS", description: "J'ai réalisé une licence dans l'informatique afin d'obtenir un Bac+3. J'ai choisi cette licence car elle mélangeait les deux domaines scientifiques qui me passionnent le plus à savoir l'informatique et les mathématiques. En 2021, j'ai obtenu ma licence en Mathématiques et Informatique Appliqués aux Sciences Humaines et Sociales." },
  { "src": "/img/parcours/UT2J_master.png", matched: false, explanation: "Master ICE-LD", description: "Je suis aujourd'hui en première année de master Ingénierie Continue pour les Ecosystèmes Logiciels et Données qui est la continuité de ma formation en licence." },
  { "src": "/img/parcours/meteo.png", matched: false, explanation: "Stage chez Météo-France", description: "Durant mon stage chez Météo-France, je me devais de tester les différentes API de diffusions sur les réseaux sociaux et de fournir un document détaillant toutes les manipulations et tous les appels aux différentes API que j’ai pu réaliser. J’ai pu tester L’API de Twitter notamment poster un tweet, supprimer un tweet, ajouter des emoticones à un tweet et poster des images. Tous ces tests se sont bien déroulés sauf celui pour poster une image. Pour contourner cette difficulté, je me suis tourné vers la bibliothèque Twurl qui m’a permis de poster une image sans trop de difficulté. Enfin, je me suis orienté vers Hootsuite qui proposait une API de publication avec la possibilité de programmer l’envoi des informations sur différents réseaux sociaux. Ce stage m’a  permis de découvrir le monde professionnel dans l’informatique au sein d’une réelle entreprise. Techniquement, ce stage m’a permis de comprendre le fonctionnement des API en général et plus particulièrement de celles pour diffuser sur les réseaux sociaux. De plus, j’ai beaucoup pratiqué l’anglais durant le stage que ce soit pour lire la documentation (souvent en anglais) ou m’adresser au support des API quand il y avait un problème. Mon apport à l’entreprise pour stage est un gain de temps car le travail que j’ai réalisé sur deux mois aurait du être réalisé par l’équipe de développement de Météo-France, qui n’en ont pas eu le temps." },
  { "src": "/img/parcours/irit.png", matched: false, explanation: "Stage à l'IRIT", description: "Durant mon stage à l'IRIT, ma massion était de réaliser une application web facilitant et automatisant le téléchargement d'archives numérisées par une recherche par mot-clé à destination des historiens. J'ai pu réaliser en Python un script de reconnaissance de mot, en Javascript et PHP un interface web permettant le téléchargement d'un dossier contenant les archives correspondantes." },
  { "src": "/img/parcours/itsap.jpg", matched: false, explanation: "Alternance à l'ITSAP", description: "Au cours de mon alternance, l’objectif de ma mission est d’intervenir sur le projet IODA en tant que développeur et plus spécifiquement sur l’interface de saisie de données pour apporter mes compétences techniques dans un premier notamment sur une migration de l’application web vers une version du framework web Symfony LTS (4.4). Dans un second temps, la correction de certaines fonctionnalités qui seraient défaillantes dues au manque de maintenance de l’application ainsi que le développement de nouvelles fonctionnalités. Et enfin dans un futur à plus long-terme, l’objectif de pouvoir croiser certaines données entre elles afin de les mettre à disposition pour la recherche." },
]

const cardImagesMotivations = [
  { "src": "/img/motivation/environnement.png", matched: false, explanation: "Environnement de travail", description: "Un cadre de travail agréable et propice au bien-être et à la productivité ne sera jamais vu comme de trop. Il est important que l'environnement de travail soit le plus attrayant possible afin de bien se sentir au sein de ce dernier vu que je vais y passer beaucoup de temps."},
  { "src": "/img/motivation/localisation.png", matched: false, explanation: "Localisation", description: "Le secteur géographique de l'entreprise est un critère important car il s'agit de l'endroit, en général où je vais vivre donc de préférence à proximité d'une grande ville pour tout avoir à coté." },
  { "src": "/img/motivation/techno.png", matched: false, explanation: "Technologies utilisées", description: "Certaines technologies sont plus attrayantes que d'autres. Selon moi, il est important de se lancer dans des technologies que l'on maitrise afin d'éviter de se retrouver coincé dans la réalisation technique." },
  { "src": "/img/motivation/salary.png", matched: false, explanation: "Rémunération" , description: "Le salaire est un critère assez important vu que c'est avec ce revenu que je vais vivre. Un salaire en deça de mes attentes pourrait être un frein dans ma prise de décision."},
  { "src": "/img/motivation/mission.png", matched: false, explanation: "Missions proposées", description: "Les missions sont la première chose que je regarde. L’ennui est mon pire cauchemar. Ce que je cherche, c’est avant tout des missions stimulantes, variées, challengeantes et sentir qu’au travers d’elles, j'aurai un réel impact au sein de l'entreprise." },
  { "src": "/img/motivation/horaire.png", matched: false, explanation: "Horaires de travail", description: "Il est important pour moi d'avoir des horaires de travail qui soient souples car avec des horaires fixes, j'ai l'impression à chaque fois de devoir compter mes heures." },
  { "src": "/img/motivation/evolution.png", matched: false, explanation: "Evolution de carrière", description: "Avec l’expérience, on acquiert de nouvelles compétences qui, en toute logique, doivent être ensuite reconnues à leur juste valeur. Je veux bâtir ma carrière et suis sensibles aux perspectives d’évolution que peut m'offrir un poste en termes d'évolution vers un poste à responsabilité par exemple."},
  { "src": "/img/motivation/image.png", matched: false, explanation: "Image corporative", description: "L'image de l'entreprise est très importante à mes yeux car je ne m'imagine pas me donner à 100% pour une aventure qui ne porte pas mes valeurs éthiques ou qui a une mauvaise réputation." }
]

const cardImagesPersonnalite = [
  { "src": "/img/personnalite/altruisme.png", matched: false, explanation: "Altruisme", description: "J'apprécie aider les autres sans forcément attendre de gratification/reconnaissance en retour. De plus je trouve une forme d'épanouissement en accomplissant des choses pour les autres. J'ai déjà effectué des dons à une oeuvre caritative, en temps et en argent."},
  { "src": "/img/personnalite/aventure.png", matched: false, explanation: "Aventure", description: "Je suis un fervent défenseur du changement, j'aime voyager ou découvrir de nouvelles expériences. Je trouve la routine ennuyeuse et je cherche de nouvelles voies pour m'épanouir en tant que personne." },
  { "src": "/img/personnalite/bienveillance.png", matched: false, explanation: "Bienveillance", description: "J'apprécie sincèrement les autres et j'exprime ces sentiments positifs. Je me fais des amis plutôt rapidement et facilement. " },
  { "src": "/img/personnalite/confiance.png", matched: false, explanation: "Confiance", description: "Je pars du principe que la plupart des personnes sont justes, honnêtes et ont de bonnes intensions. Je donne ma confiance assez facilement au début mais si je me rends compte que cette confiance est brisée, je pourrais difficilement refaire confiance." },
  { "src": "/img/personnalite/conscience.png", matched: false, explanation: "Conscience de soi", description: "Je ne souffre pas du regard ou du jugement des autres. De plus, je ne suis pas nerveux dans mes interractions sociales." },
  { "src": "/img/personnalite/cooperation.png", matched: false, explanation: "Coopération", description: "Je n'aime pas la confrontation. Je préfère faire des compromis ou renier mes propres besoins pour bien m'entendre avec les autres."},
  { "src": "/img/personnalite/enthousiasme.png", matched: false, explanation: "Enthousiasme", description: "Je m'ennuie facilement sans un niveau de stimulation. J'aime les grandes activités, l'agitation, prendre des risques et ressentir le frisson."},
  { "src": "/img/personnalite/perseverance.png", matched: false, explanation: "Perséverance", description: "Je fais tout mon possible pour atteindre l'excellence. Je continue pour être reconnu pour avoir atteint de nobles objectifs. Dans la vie de tous les jours, je m'entraine 3 fois par semaine afin d'atteindre l'objectif que je me suis fixé ainsi que pour avoir une bonne santé."},
]

const cardImagesCentres = [
  { "src": "/img/centres/football.png", matched: false, explanation: "Football", description: "Passionné par le sport en général et plus particulièrement le football, j'ai pratiqué ce sport en club pendant plus de 10 ans et que je continue de pratiquer aujourd'hui. Ce sport m'a permis de développer des compétences comme le travail d’équipe, la gestion des conflits, l'adaptation, l'écoute et la confiance en soi." },
  { "src": "/img/centres/code.png", matched: false, explanation: "Informatique", description: "J'ai découvert cette passion pour l'informatique en entrant dans l'enseignement supérieur. J'appris à découvrir un domaine aux multiples opportunités mais aussi un domaine plein de ressources qui ne font que s'enrichir chaque jour."},
  { "src": "/img/centres/technologie.png", matched: false, explanation: "Nouvelles technologies", description: "Passionné par l'informatique et donc par les nouvelles technologies, je me renseigne sur ces dernières qui font parties de notre quotidien aujourd'hui. C'est un sujet qui me passionne car on découvre des nouvelles choses quasiment tous les jours. Cette passion m'a permis de développer ma curiosité ainsi que mon sens critique." },
  { "src": "/img/centres/voyage.png", matched: false, explanation: "Voyage", description: "J'ai eu la chance de faire plusieurs voyages dans des pays étrangers comme l'Espagne, le Portugal, l'Italie, l'Angleterre, les Pays-Bas, les Etats-Unis ou encore la Martinique. Au travers de ses voyages, j'ai pu découvrir de nouvelles cultures, de nouveaux modes de vie qui sont plus ou moins différent du notre. Cela m'a permis de développer ma curiostité ainsi que mon ouverture d'esprit." },
  { "src": "/img/centres/dessin.png", matched: false, explanation: "Dessin", description: "Lorsque j'ai du temps libre, j'ai pour habitude de dessiner. J'adore cette activité car cela me permet de me détendre, me vider la tête ou encore m'évader de la réalité. Cette activité m'a permis de travailler des compétences telles que le perfectionnisme, l'esprit critique ou encore de faire attention aux détails." },
  { "src": "/img/centres/reflexion.png", matched: false, explanation: "Activités de réflexion", description: "J'adore utiliser mon cerveau afin de résoudre des problèmes. J'affectionne beaucoup les activités telles que les Escape Game, les échecs ou encore les puzzles. Cela m'a permis développer mes compétences d'observation, de raisonnement ou encore de logique." },
]

const cardImagesFormations = [
  { "src": "/img/formation/operateur.png", matched: false, explanation: "Opérateur de distribution - Pierre Fabre", description: "Durant cette mission, mon rôle était de transporter des palettes de colis pour ensuite les amener sur une filmeuse pour qu'ils puissent ensuite être chargés dans les convois de transport. Cette mission m'a fait comprendre que chaque personne dans une équipe avait un rôle important et qu'il était indispensable au bon fonctionnement de la chaine de travail."},
  { "src": "/img/formation/administration.png", matched: false, explanation: "Administration Internationnale - Pierre Fabre", description: "Durant cette mission, mon rôle était de réorganiser le système d'archivage des dossiers de l'administration internationnale de Pierre Fabre. Cela m'a permis de développer mon organision ainsi que ma rigueur car lors de cette mission je devais manipuler un nombre incalculable de dossier donc il fallait bien que je m'organise. De plus, la rigueur et la concentration ont été la clé de mon succès lors de cette mission car malgré la répétitivité des tâches, je me devais de faire attention aux dossiers que je manipulais." },
  { "src": "/img/formation/laref.png", matched: false, explanation: "Bénévole - La Ref des Campus", description: "Lors de ma 3eme année de licence MIASHS, j’ai eu l’occasion de faire partie de l’association « La Ref des campus » au sein de l’Université Toulouse Jean-Jaurès. Mon rôle, ainsi que celui de mon équipe était de développer une application web permettant de recenser les différentes associations et évènements qui pouvait avoir lieu au sein des universités de Toulouse. Pour réaliser ce travail, nous sommes une équipe de 5 développeurs qui travaille lors des créneaux horaires qui nous sont mis à disposition dans le cadre d’un projet tutoré. Pour réaliser cette application, nous avons utilisé ReactJS pour la partie Front-end et Python pour la partie Back-end." },
  { "src": "/img/formation/mathematiques.png", matched: false, explanation: "Soutien scolaire en mathématiques", description: "Lors de ma scolarité, j'ai eu l'occasion de faire du soutien scolaire notemment en mathématiques car je dispose de facilité dans cette matière. J'ai pu aider des élèves de terminale S mais aussi des élèves en Bac +1 à l'IUT de Blagnac. Cela m'a permis de travailler sur ma pédagogie ainsi que sur mon sens de l'écoute." }
]

function App() {

  const [tabFinished, setTabFinished] = useState([])
  const [cards, setCards] = useState([]);
  const [difficulty, setDifficulty] = useState(12);
  const [rubrique, setRubrique] = useState("competences");
  const [explanation, setExplanation] = useState(null);
  const [find, setFind] = useState(0);
  const [finished, setFinished] = useState(null);
  const [nbNiveaux, setNbNiveaux] = useState(0);
  const [turns, setTurns] = useState(0);
  const [choiceOne, setChoiceOne] = useState(null);
  const [choiceTwo, setChoiceTwo] = useState(null);
  const [disabled, setDisabled] = useState(false);

  // shuffle cards, duplicate cards to get set of 12, assign random ID to each
  const shuffleCards = (nb) => {
    
    let shuffledCards = [];

    if (rubrique === "competences"){
      shuffledCards = [...cardImagesCompetences.slice(0,(nb/2)), ...cardImagesCompetences.slice(0,(nb/2))]      // 2 lots of card images
      .sort(() => Math.random() - 0.5)                        // shuffled array
      .map((card) => ({ ...card, id: Math.random() }))       // add on random ID number to each card
    } else if (rubrique === "parcours"){
      shuffledCards = [...cardImagesParcours.slice(0,(nb/2)), ...cardImagesParcours.slice(0,(nb/2))]      // 2 lots of card images
      .sort(() => Math.random() - 0.5)                        // shuffled array
      .map((card) => ({ ...card, id: Math.random() }))       // add on random ID number to each card
    } else if (rubrique === "perso"){
      shuffledCards = [...cardImagesPersonnalite.slice(0,(nb/2)), ...cardImagesPersonnalite.slice(0,(nb/2))]      // 2 lots of card images
      .sort(() => Math.random() - 0.5)                        // shuffled array
      .map((card) => ({ ...card, id: Math.random() }))       // add on random ID number to each card
    } else if (rubrique === "centres"){
      shuffledCards = [...cardImagesCentres.slice(0,(nb/2)), ...cardImagesCentres.slice(0,(nb/2))]      // 2 lots of card images
      .sort(() => Math.random() - 0.5)                        // shuffled array
      .map((card) => ({ ...card, id: Math.random() }))       // add on random ID number to each card
    } else if (rubrique === "motivations"){
      shuffledCards = [...cardImagesMotivations.slice(0,(nb/2)), ...cardImagesMotivations.slice(0,(nb/2))]      // 2 lots of card images
      .sort(() => Math.random() - 0.5)                        // shuffled array
      .map((card) => ({ ...card, id: Math.random() }))       // add on random ID number to each card
    } else {
      shuffledCards = [...cardImagesFormations.slice(0,(nb/2)), ...cardImagesFormations.slice(0,(nb/2))]      // 2 lots of card images
      .sort(() => Math.random() - 0.5)                        // shuffled array
      .map((card) => ({ ...card, id: Math.random() }))       // add on random ID number to each card
    }
    
    setChoiceOne(null);
    setChoiceTwo(null);
    setCards(shuffledCards);
    setTurns(0);
  }

  // handle a user choice, update choice one or two
  const handleChoice = (card) => {
    choiceOne ? setChoiceTwo(card) : setChoiceOne(card)        // if choiceOne is null (is false), update with setChoiceOne, else update choiceTwo with setChoiceTwo
  }

  // compare two selected cards
  useEffect(() => {
    if (choiceOne && choiceTwo) {
      setDisabled(true);
      if (choiceOne.src === choiceTwo.src) {
        setCards(prevCards => {
          return prevCards.map((card) => {
            if (card.src === choiceOne.src) {
              setExplanation(<p><strong>{card.explanation} : </strong>{card.description}</p>)
              return { ...card, matched: true }
            } else {
              return card;
            }
          })
        })
        resetTurn();
        setFind((find) => find + 1);
      } else {
        setTimeout(() => resetTurn(), 1000);
      }
    }
  }, [choiceOne, choiceTwo])

  const incrementLevel = useCallback(() => {
    setNbNiveaux((nbNiveaux) => nbNiveaux + 1);
  }, []);

  useEffect(() => {
    if (cards.length !== 0){
      if (find === cards.length/2){
        setFinished(<p>Bravo tu as fini ce niveau ! Continue afin de pouvoir télécharger mon CV</p>);
        if (! tabFinished.includes(rubrique)){
          tabFinished.push(rubrique);
          setTabFinished(tabFinished)
          incrementLevel();
        }
      }
    }
  }, [find,cards,rubrique,tabFinished,incrementLevel])

  useEffect(() => {
    if (nbNiveaux === 6){
      setFinished(<p>Bravo tu as finis tous les niveaux ! Tu peux télécharger mon CV <a href={process.env.PUBLIC_URL + '/cv-timothee-aubry.pdf'} download>ici</a> !</p>);
    }
  }, [nbNiveaux])

  const newGame = () => {
    shuffleCards(difficulty);
    setFinished(null)
    setFind(0)
    setExplanation(null)
  }

  // reset game automagically
  useEffect(() => {
    newGame();
  }, [difficulty,rubrique]) // eslint-disable-next-line react-hooks/exhaustive-deps

  // reset choices and increase number of turns
  const resetTurn = () => {
    setChoiceOne(null);
    setChoiceTwo(null);
    setTurns(prevTurns => prevTurns + 1);
    setDisabled(false);
  }

  const handleDifficulty = (e) => {
    const newDifficulty = e.target.value;
    setDifficulty(newDifficulty);
    newGame();
  }

  const handleRubrique = (e) => {
    const newRubrique = e.target.value;
    setRubrique(newRubrique);
    newGame();
  }

  return (
    <div className="App">
      <div className="head">
        <h1>Timothée Aubry</h1>
        <h5>Le jeu de carte Memory</h5>
        <button onClick={newGame}>Nouvelle partie</button>
        <select onChange={(e) => handleDifficulty(e)}>
          <option value="4">Facile</option>
          <option value="8">Moyen</option>
          <option value="12" selected>Difficile</option>
          <option value="20">Expert</option>
        </select>
        <select onChange={(e) => handleRubrique(e)}>
          <option value="perso">Traits de personnalité</option>
          <option value="competences" selected>Compétences professionnelles</option>
          <option value="parcours">Parcours professionnel</option>
          <option value="centres">Centres d'intérêt</option>
          <option value="motivations">Motivations socioprofessionnelles</option>
          <option value="formation">Formations complémentaires</option>
        </select>
        <p>Coup: {turns} Niveaux terminés : {nbNiveaux}/6</p>
        {finished}
      </div>
      <div className="game">
        <div className="card-grid">
          {cards.map((card) => (
            <SingleCard
              key={card.id}
              card={card}
              handleChoice={handleChoice}
              cardFlipped={card === choiceOne || card === choiceTwo || card.matched}
              disabled={disabled}
            />
          ))}
        </div>
        <div className='explanation'>
          <h1>
            Explications
          </h1>
          <div className='description'>
            {explanation}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
